# Test App
This is an app is part of an interview task. It is a basic express js template.

## How to compile docker image.
Use the below command to compile the docker image when you are inside the "test-app" directory.
```console
docker build -t nodejs-testapp .
```